#!/usr/bin/env bash
REPO=$1
TAG=$2

docker build . -t redis-app
docker tag redis-app $REPO/redis-app:$TAG
docker push $REPO/redis-app:$TAG
