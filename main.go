package main

import (
	"fmt"
	"net/http"

	"github.com/go-redis/redis"
)

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis-master:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("connected to redis!!!! branch!! new version!", pong)

	_, err = client.Set("hitCount", 0, 0).Result()
	if err != nil {
		panic(err)
	}

	s := http.NewServeMux()

	s.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("got health! App v2!")
		w.WriteHeader(200)
	})

	s.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Got Hit!")
		client.Incr("hitCount")
		fmt.Fprint(w, "sent to redis!")
	})

	http.ListenAndServe(":80", s)
}
